<?php $__env->startSection('content'); ?>
<section class="section section-shaped section-lg my-0">
  <div class="container pt-lg-md">
    <div class="row justify-content-center">
      <div class="col-lg-9">
      	<h2 class="mb-5">
          <span>Editing Article: <?=$article->name ?></span>
        </h2>
        <div class="card bg-secondary shadow border-0">
          <div class="card-body px-lg-5 py-lg-5">
              <?php echo e(Form::model($article, array('route' => array('article_update', $article->id), 'method' => 'POST', 'enctype' => 'multipart/form-data'))); ?>

              <div class="form-group mb-3">
              	<label>Name</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                  </div>
                  <input id="name" type="text" class="form-control" name="name" value="<?=$article->name ?>" required autofocus>
                </div>
              </div>
              <div class="form-group">
              	<label>Description</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                  </div>
                  <?php echo e(Form::textarea('description', $article->description, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Article description'])); ?>

                </div>
              </div>
              <div class="form-group">
                <label>Image</label>
                <?php echo e(Form::file('feature_image')); ?>

<!--                    <input value="<?= $article->feature_image ?>" name="feature_image" id="feature_image" type="file" class="form-control-file" id="exampleFormControlFile1">     -->         
              </div>
              <div >
                <button type="submit" type="button" class="btn btn-success my-4"><?php echo e(__('Save')); ?></button>
              </div>
            <?php echo e(Form::close()); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.custom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>