<?php $__env->startSection('content'); ?>
<section class="section section-components pb-0" id="section-components">
  <div class="container">
    <div class="row justify-content-center shift-top">
      <div class="col-lg-12">
        <h2 class="mb-5">
          <span>Articles List</span>
        </h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <th scope="row"><?php echo e($article->id); ?></th>
                <td><?php echo e($article->name); ?></td>
                <td><?php echo e($article->description); ?></td>
                <td>        
                  <a href="/article/edit/<?php echo e($article->id); ?>"><button type="button" class="btn btn-default btn-sm">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                  </button></a>
              </td>
              <td>              
                <a href="/article/delete/<?php echo e($article->id); ?>"><button type="submit" class="btn btn-default btn-sm">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
              </a>
              </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.custom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>