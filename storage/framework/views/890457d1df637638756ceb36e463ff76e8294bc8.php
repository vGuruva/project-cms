<?php $__env->startSection('content'); ?>
<section class="section section-components pb-0" id="section-components">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-12">
        <h2 class="mb-5">
          <span>Categories List</span>
        </h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <th scope="row"><?php echo e($category->id); ?></th>
                <td><?php echo e($category->name); ?></td>
                <td><?php echo e($category->description); ?></td>
                <td>        
                  <a href="/category/edit/<?php echo e($category->id); ?>"><button type="button" class="btn btn-default btn-sm">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                  </button></a>
              </td>
              <td>
                <?php echo Form::open(['action' => ['Category\CategoryController@destroy', $category->id], 'method' => 'POST', 'class' => 'pull-right']); ?>

                <button type="submit" class="btn btn-default btn-sm">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
                <?php echo Form::close(); ?>

              </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
         <?php echo e($categories->links()); ?>

      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.custom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>