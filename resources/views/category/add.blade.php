@extends('layouts.custom')

@section('content')
<section class="section section-shaped section-lg my-0">
  <div class="container pt-lg-md">
    <div class="row justify-content-center">
      <a href="{{ url('categories') }}"><button class="btn btn-1 btn-outline-primary" type="button">All categories</button></a> 
      <a style="margin-left: 10px;" href="{{ url('articles') }}"><button class="btn btn-1 btn-outline-primary" type="button">Add articles</button></a>
      <div class="col-lg-9">
      	<h2 class="mb-5">
          <span>Add Category</span>
        </h2>
        <div class="card bg-secondary shadow border-0">
          <div class="card-body px-lg-5 py-lg-5">
            {!! Form::open(['action' => 'CategoryController@store', 'method' => 'POST']) !!}
                @csrf
              <div class="form-group mb-3">
              	<label>Name</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                  </div>
                   {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Category name'])}}
                </div>
              </div>
              <div class="form-group">
              	<label>Description</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                  </div>
                  {{Form::textarea('description', '', ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Category description'])}}
                </div>
              </div>
              <div>
                <button type="submit" type="button" class="btn btn-success my-4">{{ __('Save') }}</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
