@extends('layouts.custom')

@section('content')
<section class="section section-shaped section-lg my-0">
  <div class="container pt-lg-md">
    <div class="row justify-content-center">
      <div class="col-lg-9">
      	<h2 class="mb-5">
          <span>Editing category: <?=$category->name ?></span>
        </h2>
        <div class="card bg-secondary shadow border-0">
          <div class="card-body px-lg-5 py-lg-5">
              {{ Form::model($category, array('route' => array('category_update', $category->id), 'method' => 'POST')) }}
              <div class="form-group mb-3">
              	<label>Name</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                  </div>
                  <input id="name" type="text" class="form-control" name="name" value="<?=$category->name ?>" required autofocus>
                </div>
              </div>
              <div class="form-group">
              	<label>Description</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                  </div>
                  {{Form::textarea('description', $category->description, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Category description'])}}
                </div>
              </div>
              <div >
                <button type="submit" type="button" class="btn btn-success my-4">{{ __('Save') }}</button>
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
