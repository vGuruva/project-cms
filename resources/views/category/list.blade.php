@extends('layouts.custom')

@section('content')
<section class="section section-components pb-0" id="section-components">
  <div class="container">
    <div class="row justify-content-center shift-top">
      <a href="{{ url('articles') }}"><button class="btn btn-1 btn-outline-primary" type="button">All Articles</button></a> 
      <a style="margin-left: 10px;" href="{{ url('category/add') }}"><button class="btn btn-1 btn-outline-primary" type="button">Add Category</button></a>
      <div class="col-lg-12">
        <h2 class="mb-5">
          <span>Category List</span>
        </h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th style="width: 40px;"  scope="col">Articles</th>
              <th style="width: 40px;"  scope="col">Edit</th>
              <th style="width: 40px;"  scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
              @foreach($categories as $category)
            <tr>
              <th scope="row">{{ $category->id }}</th>
                <td>{{ $category->name }}</td>
                <td>{{ $category->description }}</td>
                <td>        
                  <a href="/category/{{ $category->id }}/articles">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                  </a>
                </td>
                <td>        
                  <a href="/category/edit/{{ $category->id }}">
                    <button type="button" class="view btn btn-default btn-sm">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                  </a>
              </td>
              <td>
                {!!Form::open(['action' => ['CategoryController@destroy', $category->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                    <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>
                {!!Form::close()!!}                 
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>         
      </div>
    </div>
    {{ $categories->links() }}
  </div>
</section>
@endsection
