@extends('layouts.custom')

@section('content')
<section class="section section-components pb-0" id="section-components">
  <div class="container">

    <div class="row justify-content-center shift-top">
            <a href="{{ url('categories') }}"><button class="btn btn-1 btn-outline-primary" type="button">All Categories</button></a>
            <a style="margin-left: 10px;" href="{{ url('article/add') }}"><button class="btn btn-1 btn-outline-primary" type="button">Add Article</button></a>
      <div class="col-lg-12">
        <h2 class="mb-5">
          <span>Articles List</span>
        </h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th style="width: 40px;"  scope="col">View</th>
              <th style="width: 40px;"  scope="col">Edit</th>
              <th style="width: 40px;"  scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
              @foreach($articles as $article)
            <tr>
              <th scope="row">{{ $article->id }}</th>
                <td>{{ $article->name }}</td>
                <td>{{ $article->description }}</td>
                <td>        
                  <a href="/article/show/{{ $article->id }}">
                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>
                  </a>
                </td>
                <td>
                @if(!Auth::guest())
                  @if(Auth::user()->id == $article->user_id)        
                  <a href="/article/edit/{{ $article->id }}">
                    <button type="button" class="view btn btn-default btn-sm">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                  </a>
                  @endif
                @endif 
              </td>
              <td>
            @if(!Auth::guest())
              @if(Auth::user()->id == $article->user_id)
                {!!Form::open(['action' => ['ArticleController@destroy', $article->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                    <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>
                {!!Form::close()!!} 
              @endif
            @endif                 
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>         
      </div>
    </div>
    {{ $articles->links() }}
  </div>
</section>
@endsection
