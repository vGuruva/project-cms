<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Article;

class Category extends Model
{
	use SoftDeletes;

	protected $date = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'description',
    ];

    public function articles(){
    	return $this->hasMany(Article::class);
    }
}
