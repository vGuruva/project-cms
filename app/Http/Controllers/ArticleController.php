<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::orderBy('created_at','desc')->paginate(10);
        return view("article/list", ["articles" => $articles]);

    }

    public function create()
    {
        $categories = Category::all();
        return view("article/add", ['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'feature_image' => 'image|nullable|max:1999'
        ];

        if($request->hasFile('feature_image')){

            $filenameWithExt = $request->file('feature_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('feature_image')->getClientOriginalExtension();
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            $path = $request->file('feature_image')->storeAs('public/feature_images', $fileNameToStore);
        } else {
            $fileNameToStore = 'image_null.jpg';
        }  

        $article = new Article;
        $article->name = $request->input('name');
        $article->description = $request->input('description');
        $article->user_id = auth()->user()->id;
        $article->category_id = $request->input('category_id');
        $article->feature_image = $fileNameToStore;
        $article->save();

        return view('article/show')->with('article', $article);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('article/show')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'required',
            'description'=>'required'
        ]);

        if($request->hasFile('feature_image')){
            $filenameWithExt = $request->file('feature_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('feature_image')->getClientOriginalExtension();
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            $path = $request->file('feature_image')->storeAs('public/feature_images', $fileNameToStore);
        }

        $article = Article::find($id);
        $article->name = $request->input('name');
        $article->description = $request->input('description');
        $article->category_id = $request->input('category_id');

        if($request->hasFile('feature_image')){
            $article->feature_image = $fileNameToStore;
        }

        $article->save();

        return view('article/show')->with('article', $article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);

        if(auth()->user()->id !==$article->user_id){
            return redirect('articles')->with('error', 'Unauthorized Page');
        }

        if($article->feature_image != 'image_null.jpg'){
            Storage::delete('public/feature_images/'.$article->feature_image);
        }
        
        $article->delete();
        return redirect('articles')->with('success', 'Article deleted');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();
        if(auth()->user()->id !==$article->user_id){
            return redirect('articles')->with('error', 'Unauthorized Page');
        }

        $allCategories = [];
        foreach ($categories as $key => $cat) {
            if($cat->id != $article->category->id):
                $allCategories[$cat->id] = $cat->name;
            endif;
        }

        return view('article/edit',['article'=>$article, 'allCategories'=>$allCategories]);
    }


}
