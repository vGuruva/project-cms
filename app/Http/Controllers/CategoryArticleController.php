<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
    	$category = Category::find($id);
        $articles = $category->articles;

        return view("category/articles", ["articles" => $articles, 'category'=>$category]);


    }
}
