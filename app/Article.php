<?php

namespace App;

use App\User;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

	protected $date = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'description',
    ];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(User::class);
    }

}
