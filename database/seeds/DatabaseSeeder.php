<?php
use App\User;
use App\Article;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Article::truncate();
        Category::truncate();

        $usersQuantity = 10;
        $articlesQuantity = 30;
        $categoriesQuantity = 15;

        factory(User::class, $usersQuantity)->create();
        factory(Category::class, $categoriesQuantity)->create();
        factory(Article::class, $articlesQuantity)->create();
        
    }
}
