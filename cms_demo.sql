-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: cms
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `feature_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'aut','Nihil ea sint dolore est at ipsam. Animi velit a officia ullam adipisci.',6,2,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(2,'fugit','Quaerat et rerum accusamus velit ea.',8,5,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(3,'nobis','Rem occaecati natus quidem ut quibusdam eius quia.',6,4,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(4,'odio','Optio temporibus commodi impedit eligendi ut.',7,8,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(5,'nihil','Qui numquam mollitia molestiae.',10,10,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(6,'dolor','Nesciunt vel quas adipisci eum pariatur nobis.',6,13,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(7,'ut','Et blanditiis harum nobis.',9,10,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(8,'necessitatibus','Et consectetur quo soluta debitis id ad nihil sed.',9,14,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(9,'nemo','Dignissimos fuga aut deserunt cupiditate nemo. Quam culpa tenetur eius nulla provident eius consectetur.',8,7,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(10,'recusandae','Est laborum voluptas nulla.',5,5,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(11,'maiores','Beatae reprehenderit possimus quia neque nisi quo dignissimos.',8,5,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(12,'sint','Ut quos distinctio debitis.',8,5,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(13,'quaerat','Atque culpa occaecati occaecati architecto molestiae odio. Ipsum facere non est eius asperiores a.',3,2,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(14,'soluta','Et sint aperiam sunt sunt a ut qui.',7,9,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(15,'commodi','Aut consequuntur reprehenderit voluptas commodi.',5,5,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(16,'soluta','Accusamus laborum numquam beatae accusantium. Dolorum minus autem sint voluptate ut voluptatem.',5,15,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(17,'aut','Inventore nihil commodi et ullam quia.',4,10,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(18,'est','Et similique velit sunt pariatur hic perferendis. Est eum recusandae tempore provident velit placeat nemo.',10,7,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(19,'omnis','Provident ipsam mollitia ab expedita voluptatem. Voluptatum facere voluptatum unde quam voluptatibus.',5,4,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(20,'molestias','Velit facere id sed rerum.',8,10,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(21,'aliquam','Explicabo sed quia totam facere quia possimus minus.',8,14,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(22,'dolorum','Et est rem repellendus nihil.',4,4,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(23,'delectus','Atque ut est aliquam nemo dolorum ad ut.',2,8,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(24,'aspernatur','Nihil dolor dolor nihil ut voluptas quis esse eos. Et quod magnam non molestiae.',10,12,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(25,'ex','Ad sunt quae ex dolor.',1,15,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(26,'porro','Consequuntur totam corrupti sed consequatur. Dolorem vel qui aliquam architecto necessitatibus dicta provident facilis.',8,2,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(27,'mollitia','Numquam enim vel dolorem a fugiat rerum velit labore.',8,2,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(28,'magni','Ut eum ullam accusantium ut sapiente.',2,4,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(29,'sint','Optio itaque quisquam dicta. Harum ipsa quis perferendis.',9,13,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(30,'recusandae','Quibusdam porro eaque impedit est molestiae blanditiis dolores.',9,1,'image_null.jpg','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(31,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712231.jpg','2018-08-19 18:57:11','2018-08-19 18:57:11',NULL),(32,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712312.jpg','2018-08-19 18:58:32','2018-08-19 18:58:32',NULL),(33,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712375.jpg','2018-08-19 18:59:35','2018-08-19 18:59:35',NULL),(34,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712397.jpg','2018-08-19 18:59:57','2018-08-19 18:59:57',NULL),(35,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712418.jpg','2018-08-19 19:00:18','2018-08-19 19:00:18',NULL),(36,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712459.jpg','2018-08-19 19:00:59','2018-08-19 19:00:59',NULL),(37,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712491.jpg','2018-08-19 19:01:31','2018-08-19 19:01:31',NULL),(38,'zimvine 4','asdsadDD',11,8,'RF2104102_20170225_3rd-ed_Kenya_65_TAR small_1534712689.jpg','2018-08-19 19:04:49','2018-08-19 19:04:49',NULL),(39,'dsadsadsad','dsadsadassadsadsadsa',11,5,'RF2158082_20180223_IMG_015_ste small_1534713602.jpg','2018-08-19 19:20:02','2018-08-19 19:20:09',NULL);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'dignissimos','Et consequatur fugit iusto reiciendis ducimus qui. sadsad','2018-08-19 17:27:46','2018-08-19 19:21:24',NULL),(2,'quo','Unde nobis sequi qui sunt cum vel eveniet quaerat. Deserunt rerum necessitatibus incidunt ab.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(3,'aspernatur','Dolor ratione rerum similique molestiae quo. Rerum sint dolorem facere similique voluptatem quidem consectetur quia.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(4,'dolor','Similique non ratione porro eos alias assumenda quae.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(5,'animi','Tempora numquam at molestiae. Dolorem consequuntur hic aliquam suscipit possimus eos.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(6,'iste','Consequatur non iste velit consequatur.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(7,'eos','Reprehenderit dicta ipsum impedit est tempora veniam. Impedit ea nulla est laudantium quos.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(8,'perspiciatis','Velit distinctio velit beatae sit eos aut mollitia. Sed quisquam nostrum quis laborum.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(9,'asperiores','Voluptate voluptatem quaerat est. Voluptas a minima quod quidem est est hic aspernatur.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(10,'ex','Quaerat est sint et odit dicta corporis.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(11,'facilis','Doloremque temporibus quia accusamus nam consectetur rem vitae. Officia quia quas omnis incidunt quidem delectus.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(12,'natus','Aspernatur et quis delectus illo excepturi pariatur beatae distinctio. Laudantium ut voluptatem omnis et aspernatur optio voluptas.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(13,'voluptates','Nemo nobis voluptatem eius incidunt ullam.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(14,'cumque','Voluptatem ea velit nobis quo et et officia. Vel odio sapiente omnis impedit.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL),(15,'fuga','Tempora eveniet consequatur ut dolorum.','2018-08-19 17:27:46','2018-08-19 17:27:46',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_08_17_140338_articles',1),(4,'2018_08_19_182330_categories_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Dr. Hertha Hilpert','kunze.gregoria@example.net','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','j3uyV3szah','2018-08-19 17:27:46','2018-08-19 17:27:46'),(2,'Candido Willms','carolina15@example.org','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','ZAALPc9esu','2018-08-19 17:27:46','2018-08-19 17:27:46'),(3,'Erich Barrows','cheyenne78@example.com','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','mtRjaAqHc4','2018-08-19 17:27:46','2018-08-19 17:27:46'),(4,'Mittie Rice','spencer63@example.com','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','ZIOddsiYdi','2018-08-19 17:27:46','2018-08-19 17:27:46'),(5,'Yolanda Erdman','hudson.jordan@example.com','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','gCEt5RLUmY','2018-08-19 17:27:46','2018-08-19 17:27:46'),(6,'Orland Schuster','manuel05@example.org','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','U9K8fKIYRz','2018-08-19 17:27:46','2018-08-19 17:27:46'),(7,'Dr. Pablo Bergstrom PhD','ndach@example.net','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','u9nrQS2B5R','2018-08-19 17:27:46','2018-08-19 17:27:46'),(8,'Dr. Cassie Prohaska I','hwilliamson@example.net','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','e2CxnA31sR','2018-08-19 17:27:46','2018-08-19 17:27:46'),(9,'Colin Muller','andrew40@example.com','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','bYsn7J3cYn','2018-08-19 17:27:46','2018-08-19 17:27:46'),(10,'Nichole Jakubowski','golda20@example.net','$2y$10$YEhMTNypGgK1rIdJF8486.KCCskFhNoXcKWn.13/bqMz0290J/z6O','AmGBM00r4M','2018-08-19 17:27:46','2018-08-19 17:27:46'),(11,'Danai Guruva','danishto@gmail.com','$2y$10$TiCF5PcE.s5KqV7yq3EZnOifC4erVkPoF58KGAQwcuQ9iMai5YLFe','9xlLPt56BUAK8PFNsAXLurumARjqOPCwCxqEzMoRxgBSyEB1cNoJ3QkBk0zT','2018-08-19 17:28:14','2018-08-19 17:28:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-19 23:23:58
