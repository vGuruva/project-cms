## CMS Project

Simple csm system that allows users to register then add articles to the list of articles

- Visitors can on view articles after registering as a user.
- Users are able to see the list of all articles contained in the cms.
- Users are only able to edit and delete articles they have created.

## Technologies powering CMS

The following technologies were used to contrust the cms:

- Laravel 5.6
- Node v 8.10 and npm v 6.1
- Bootrap 4
- Webpack and laravel-mix to bundle project resources

Please run php artisan storage:link to create symlink for image resouces. You may also run the database seeder command to fill your database with arbitrary data. Have attached a demo database cms_demo.sql.


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
